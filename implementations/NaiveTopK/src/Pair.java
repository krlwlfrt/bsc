/**
 * A pair
 */
public class Pair implements Comparable<Pair> {
    private String key;
    private Integer value;

    /**
     * Create a new pair
     *
     * @param key   The key of the pair
     * @param value The value of the pair
     */
    public Pair(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Get key of pair
     *
     * @return The key
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Get value of pair
     *
     * @return The value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * Set value of pair
     *
     * @param value The value
     */
    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * Compare to other pair
     * @param p Pair to compare to
     * @return Comparison result
     */
    public int compareTo(Pair p) {
        return p.getValue().compareTo(this.value);
    }

    /**
     * Stringify pair
     * @return Stringified pair
     */
    public String toString() {
        return this.key + ": " + this.value;
    }
}
