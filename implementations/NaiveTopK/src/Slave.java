import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A slave
 */
public class Slave {
    private List<Pair> list = new ArrayList<>();

    /**
     * Add a pair to the list of the slave
     *
     * @param p The pair to add
     */
    public void addPair(Pair p) {
        boolean contained = false;

        // Make sure we don't have doubled keys in the list
        for(Pair pairInList: list) {
            if(pairInList.getKey().equals(p.getKey())) {
                contained = true;
                pairInList.setValue(pairInList.getValue() + p.getValue());
                break;
            }
        }

        if(!contained) {
            list.add(p);
        }
    }

    /**
     * Get the top-k items of the slave
     *
     * @param k Amount of items to get
     * @return The top-k items of the slave
     */
    public List<Pair> getTopK(int k) {
        Collections.sort(list);

        int i = 0;
        List<Pair> topK = new ArrayList<>();
        while (i < k && i < list.size()) {
            Pair p = list.get(i);
            topK.add(new Pair(p.getKey(), p.getValue()));
            i++;
        }

        return topK;
    }

    /**
     * Get the items that have higher values than a lower bound
     *
     * @param lowerBound Lower bound
     * @return List of pairs with higher values
     */
    public List<Pair> getPairsWithHigherValues(Integer lowerBound) {
        Collections.sort(list);

        List<Pair> pairsWithHigherValues = new ArrayList<>();
        int i = 0;
        while (i < list.size() && list.get(i).getValue() >= lowerBound) {
            Pair p = list.get(i);
            pairsWithHigherValues.add(new Pair(p.getKey(), p.getValue()));
            i++;
        }

        return pairsWithHigherValues;
    }

    /**
     * Get a list of pairs that have the given keys
     *
     * @param keys A list of keys
     * @return List of pairs that have the given keys
     */
    public List<Pair> getPairsWithKeys(List<String> keys) {
        Collections.sort(list);

        List<Pair> pairsWithKeys = new ArrayList<>();
        for (Pair p : list) {
            if (keys.contains(p.getKey())) {
                pairsWithKeys.add(new Pair(p.getKey(), p.getValue()));
            }
        }

        return pairsWithKeys;
    }

    /**
     * Get amount of pairs
     * @return Amount of pairs
     */
    public Integer getAmountOfPairs() {
        return list.size();
    }
}
