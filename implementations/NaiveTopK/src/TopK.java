import java.util.List;
import java.util.Random;

/**
 * A top-k implementation as proposed in http://www.dblab.ntua.gr/~gtsat/collection/topK/distr%20top%20k/topk.pdf
 */
public class TopK {
    public static void main(String[] args) {
        int slaves = 10;
        int minPairTries = 26;
        int maxPairTries = 62;

        Random generator = new Random();

        Master m = new Master();

        for (int slaveI = 0; slaveI < slaves; slaveI++) {
            Slave s = new Slave();

            for (int pairI = 0; pairI < generator.nextInt(maxPairTries - minPairTries) + minPairTries; pairI++) {
                int asciiValue = generator.nextInt(26) + 65;
                char c = (char) asciiValue;
                s.addPair(new Pair(Character.toString(c), generator.nextInt(500)));
            }

            System.out.println("Created slave " + slaveI + " with " + s.getAmountOfPairs() + " pairs.");

            m.addSlave(s);
        }

        System.out.println();
        System.out.println("Starting top k");
        List<Pair> top3 = m.getTopK(3);
        System.out.println();
        System.out.println("Finished top k");


        System.out.println();
        System.out.println("List of top k items");
        System.out.println(top3);
    }
}
