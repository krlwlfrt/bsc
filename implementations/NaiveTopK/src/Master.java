import java.util.*;

/**
 * A master
 */
public class Master {
    private List<Slave> slaves = new LinkedList<>();

    /**
     * Add a slave to the list of slaves
     *
     * @param s The slave to add
     */
    public void addSlave(Slave s) {
        slaves.add(s);
    }

    /**
     * Get the top-k items of the slaves
     *
     * @param k Amount of items to get
     * @return The top-k items of the slaves
     */
    public List<Pair> getTopK(Integer k) {
        List<Pair> partialSums = new ArrayList<>();
        for (Slave s : slaves) {
            for (Pair partialP : s.getTopK(k)) {
                boolean contained = false;

                for (Pair p : partialSums) {
                    if (partialP.getKey().equals(p.getKey())) {
                        p.setValue(partialP.getValue() + p.getValue());
                        contained = true;
                        break;
                    }
                }

                if (!contained) {
                    partialSums.add(partialP);
                }
            }
        }

        Collections.sort(partialSums);

        System.out.println();
        System.out.println("Partial sums after first step");
        System.out.println(partialSums);

        Integer tau1 = partialSums.get(partialSums.size() - 1).getValue();

        System.out.println();
        System.out.println("tau1 is " + tau1);

        List<Pair> partialSumsTau1 = new ArrayList<>();
        for (Slave s : slaves) {
            for (Pair partialP : s.getPairsWithHigherValues(tau1)) {
                boolean contained = false;

                for (Pair p : partialSumsTau1) {
                    if (partialP.getKey().equals(p.getKey())) {
                        p.setValue(partialP.getValue() + p.getValue());
                        contained = true;
                        break;
                    }
                }

                if (!contained) {
                    partialSumsTau1.add(partialP);
                }
            }
        }

        Collections.sort(partialSumsTau1);

        System.out.println();
        System.out.println("Partial sums after second step");
        System.out.println(partialSumsTau1);

        Integer tau2 = partialSumsTau1.get(partialSumsTau1.size() - 1).getValue();

        System.out.println();
        System.out.println("tau2 is " + tau2);

        List<String> keys = new ArrayList<>();
        for (Pair p : partialSumsTau1) {
            keys.add(p.getKey());
        }

        List<Pair> partialSumsTau2 = new ArrayList<>();
        for (Slave s : slaves) {
            for (Pair partialP : s.getPairsWithKeys(keys)) {
                boolean contained = false;

                for (Pair p : partialSumsTau2) {
                    if (partialP.getKey().equals(p.getKey())) {
                        p.setValue(partialP.getValue() + p.getValue());
                        contained = true;
                        break;
                    }
                }

                if (!contained) {
                    partialSumsTau2.add(partialP);
                }
            }
        }

        Collections.sort(partialSumsTau2);

        System.out.println();
        System.out.println("Sums after third step");
        System.out.println(partialSumsTau2);

        int i = 0;
        List<Pair> topK = new ArrayList<>();
        while (i < k && i < partialSumsTau2.size()) {
            Pair p = partialSumsTau2.get(i);
            topK.add(new Pair(p.getKey(), p.getValue()));
            i++;
        }

        return topK;
    }
}
