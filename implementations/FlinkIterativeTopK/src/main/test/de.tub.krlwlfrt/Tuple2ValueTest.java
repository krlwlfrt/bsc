package de.tub.krlwlfrt;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tuple2ValueTest {
    @Test
    public void getterTest () {
        Tuple2<String, Integer> mytupl = Tuple2.of("as", 1);
        Tuple2Value fubar = new Tuple2Value(mytupl);
        assertEquals(fubar.getValue(), mytupl);
    }

    @Test
    public void setterTest () {
        Tuple2<String, Integer> myTupl = Tuple2.of("as", 1);
        Tuple2<String, Integer> myOtherTupl = Tuple2.of("cd", 2);
        Tuple2Value fubar = new Tuple2Value(myTupl);
        fubar.setValue(myOtherTupl);
        assertEquals(fubar.getValue(), myOtherTupl);
    }
}
