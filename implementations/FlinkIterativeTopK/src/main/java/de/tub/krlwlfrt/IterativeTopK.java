package de.tub.krlwlfrt;

import org.apache.flink.api.common.aggregators.ConvergenceCriterion;
import org.apache.flink.api.common.functions.*;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.IterativeDataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class IterativeTopK {

    public static void main(String[] args) throws Exception {
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        ParameterTool parameter = ParameterTool.fromArgs(args);

        final int k = parameter.getInt("k", 5);
        final int nodes = parameter.getInt("nodes", 9);
        final String file = parameter.get("file", "../data/data-1-0");
        final int elementsPerPartition = (int) Math.pow(26, 3);

        env.setParallelism(nodes);

        // read the data
        List<Tuple2<String, Integer>> elements = new LinkedList<Tuple2<String, Integer>>();
        Scanner s = new Scanner(new File(file));
        while (s.hasNext()) {
            String key = s.next();
            Integer value = Integer.parseInt(s.next());
            elements.add(Tuple2.of(key, value));
        }
        s.close();

        // partition the data
        DataSet<Tuple2<String, Integer>> sources = env.fromCollection(elements).partitionCustom(new Partitioner<Integer>() {
            int i = 0;

            @Override
            public int partition(Integer freq, int parallelism) {
                assert parallelism == nodes;

                return i++ / elementsPerPartition;
            }
        }, 1).sortPartition(1, Order.DESCENDING);

        IterativeDataSet<Tuple2<String, Integer>> it = sources.iterate(elementsPerPartition);

        it.registerAggregationConvergenceCriterion("lists", new ListAggregator(k, nodes), new ConvergenceCriterion<Tuple2ListValue>() {
            boolean convergedInLastIteration = false;

            @Override
            public boolean isConverged(int iteration, Tuple2ListValue tuple2ListValue) {
                if (convergedInLastIteration) {
                    return true;
                }

                if (tuple2ListValue != null
                        && tuple2ListValue.getList() != null
                        && tuple2ListValue.getList().size() > 0
                        && tuple2ListValue.getList().get(0).getValue().getField(0).equals("CONVERGED")) {

                    Logger.log();
                    Logger.log();
                    Logger.log("Converged in iteration " + iteration);
                    Logger.log();

                    convergedInLastIteration = true;
                }

                return false;
            }
        });

        DataSet<Tuple2<String, Integer>> newElements = it.flatMap(new RichFlatMapFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
            private ListAggregator tuple2ListAggregator;

            private int iteration, countElements, elementsPerIteration = k;

            private Tuple2ListValue possibleTopK;

            public void open(Configuration parameters) {
                // reset counted elements
                this.countElements = 0;

                // get iteration/super step number from runtime
                iteration = getIterationRuntimeContext().getSuperstepNumber() - 1;

                tuple2ListAggregator = getIterationRuntimeContext().getIterationAggregator("lists");
                tuple2ListAggregator.setGlobal(false);

                possibleTopK = getIterationRuntimeContext().getPreviousIterationAggregate("lists");

                //Logger.log("RichFlatMapFunction#open " + possibleTopK);
            }

            @Override
            public void flatMap(Tuple2<String, Integer> tuple, Collector<Tuple2<String, Integer>> out) throws Exception {
                Tuple2Value<String, Integer> tuple2Value = new Tuple2Value<>(tuple);

                boolean converged = false;

                if (possibleTopK != null && possibleTopK.getList() != null && possibleTopK.getList().size() > 0) {
                    converged = possibleTopK.getList().get(0).getValue().getField(0).equals("CONVERGED");
                }

                if (converged) {
                    if (possibleTopK.getList().size() > 1) {
                        Tuple2<String, Integer> tupleToCollect = possibleTopK.getList().remove(1).getValue();
                        if (!tupleToCollect.getField(0).equals("CONVERGED")) {
                            out.collect(Tuple2.of((String) tupleToCollect.getField(0), -1));
                            Logger.log("TAKING " + tupleToCollect);
                        }
                        return;
                    }
                    return;
                } else if ((countElements >= (iteration) * elementsPerIteration) && (countElements < (iteration + 1) * elementsPerIteration)) {
                    tuple2ListAggregator.aggregate(tuple2Value);
                }

                this.countElements++;
                out.collect(tuple);
            }
        });

        DataSet<Tuple2<String, Integer>> topKCandidates = it.closeWith(newElements);

        DataSet<Tuple2<String, Integer>> topK = sources
                .union(topKCandidates)
                .setParallelism(1)
                .groupBy(0)
                .reduceGroup(new GroupReduceFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
                    @Override
                    public void reduce(Iterable<Tuple2<String, Integer>> iterable, Collector<Tuple2<String, Integer>> collector) throws Exception {
                        boolean hadNegativeElement = false;
                        int sum = 0;
                        String key = null;

                        for (Tuple2<String, Integer> tuple : iterable) {
                            if (key == null) {
                                key = tuple.getField(0);
                            }

                            int value = tuple.getField(1);

                            if (value == -1) {
                                hadNegativeElement = true;
                            } else {
                                sum += value;
                            }
                        }

                        if (hadNegativeElement) {
                            collector.collect(Tuple2.of(key, sum));
                        }
                    }
                })
                .setParallelism(1)
                .sortPartition(1, Order.DESCENDING);

        topK.print();
    }
}
