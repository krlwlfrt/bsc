package de.tub.krlwlfrt;

import org.apache.flink.core.memory.DataInputView;
import org.apache.flink.core.memory.DataOutputView;
import org.apache.flink.types.Value;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Tuple2ListValue implements Value {
    private List<Tuple2Value<String, Integer>> list;

    public Tuple2ListValue() {
        this.list = new LinkedList<>();
    }

    public Tuple2ListValue(List<Tuple2Value<String, Integer>> list) {
        this.list = new LinkedList<>();
        this.list.addAll(list);
    }

    public List<Tuple2Value<String, Integer>> getList() {
        return this.list;
    }

    public void addAll(Tuple2ListValue tuple2ListValue) {
        this.list.addAll(tuple2ListValue.getList());
    }

    public void add(Tuple2Value tuple2Value) {
        this.list.add(tuple2Value);
    }

    @Override
    public void write(DataOutputView out) throws IOException {
        if (list.size() > Integer.MAX_VALUE) {
            throw new IOException("Length of list must not be longer than " + Integer.MAX_VALUE);
        }

        out.writeInt(list.size());

        for (Tuple2Value tuple2Value : list) {
            tuple2Value.write(out);
        }
    }

    @Override
    public void read(DataInputView in) throws IOException {
        int length = in.readInt();

        for (int i = 0; i < length; i++) {
            Tuple2Value tuple2Value = new Tuple2Value();
            tuple2Value.read(in);
            list.add(tuple2Value);
        }
    }

    public String toString() {
        return this.list.toString();
    }
}
