package de.tub.krlwlfrt;

import org.apache.flink.api.common.aggregators.Aggregator;
import org.apache.flink.api.java.tuple.Tuple2;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ListAggregator implements Aggregator<Tuple2ListValue> {
    private boolean global = true, converged = false, dirty = false;
    private Tuple2ListValue aggregatedList = new Tuple2ListValue();
    private int k, nodes, threshold;

    public ListAggregator(int k, int nodes) {
        this.k = k;
        this.nodes = nodes;
        this.threshold = 0;
    }

    @Override
    public Tuple2ListValue getAggregate() {
        if (this.global && this.dirty) {
            List<Tuple2<String, Integer>> deduplicatedList = new LinkedList();

            for (Tuple2Value tuple2Value : this.aggregatedList.getList()) {
                Tuple2<String, Integer> tuple2 = tuple2Value.getValue();

                boolean existed = false;

                for (Tuple2 existingTuple2 : deduplicatedList) {
                    if (tuple2.f0.equals(existingTuple2.f0)) {
                        existingTuple2.f1 = (Integer) existingTuple2.f1 + tuple2.f1;
                        existed = true;
                        break;
                    }
                }

                if (!existed) {
                    deduplicatedList.add(tuple2);
                }
            }

            deduplicatedList.sort(new Comparator<Tuple2<String, Integer>>() {
                @Override
                public int compare(Tuple2<String, Integer> t1, Tuple2<String, Integer> t2) {
                    return t2.f1 - t1.f1;
                }
            });

            if (threshold == 0) {
                threshold = deduplicatedList.get(k - 1).f1 / nodes;
            }

            this.aggregatedList = new Tuple2ListValue();

            if (this.converged) {
                Logger.log("ListAggregator#getAggregate PUTTING CONVERGED ELEMENT");
                this.aggregatedList.add(new Tuple2Value(Tuple2.of("CONVERGED", 0)));
            }

            int added = 0;
            for (Tuple2 tuple2 : deduplicatedList) {

                if (!this.converged || added < this.k) {
                    added++;
                    this.aggregatedList.add(new Tuple2Value(tuple2));
                }
            }

            Logger.log("ListAggregator#aggregate generated" + this.aggregatedList);
            Logger.log();
            Logger.log();

            this.dirty = false;
        }

        return this.aggregatedList;
    }

    @Override
    public void aggregate(Tuple2ListValue tuple2ListValue) {
        if (this.global) {
            this.dirty = true;

            List<Tuple2Value<String, Integer>> list = tuple2ListValue.getList();

            Logger.log(list + "" + threshold);

            if (list.size() > 0) {
                Integer value = (Integer) list.get(list.size() - 1).getValue().getField(1);

                if (value < threshold) {
                    Logger.log("ListAggregator#aggregate converged with " + value);
                    this.converged = true;
                }
            }
        }

        this.aggregatedList.addAll(tuple2ListValue);
    }

    public void aggregate(Tuple2Value tuple2Value) {
        this.aggregatedList.add(tuple2Value);
    }

    @Override
    public void reset() {
        if (!this.global) {
            this.aggregatedList.getList().clear();
        }
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }
}
