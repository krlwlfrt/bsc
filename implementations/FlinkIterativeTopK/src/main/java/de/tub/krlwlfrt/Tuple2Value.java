package de.tub.krlwlfrt;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.core.memory.DataInputView;
import org.apache.flink.core.memory.DataOutputView;
import org.apache.flink.types.Value;

import java.io.IOException;

public class Tuple2Value<K, V> extends Tuple2 implements Value {
    private Tuple2<K, V> tuple;

    public Tuple2Value() {
    }

    public Tuple2Value(Tuple2<K, V> tuple) {
        this.tuple = tuple;
    }

    public Tuple2 getValue() {
        return this.tuple;
    }

    public void setValue(Tuple2<K, V> newTuple) {
        this.tuple = newTuple;
    }

    @Override
    public void write(DataOutputView out) throws IOException {
        String key = this.tuple.getField(0);
        int stringLength = key.length();
        Integer value = this.tuple.getField(1);

        if (stringLength > Integer.MAX_VALUE) {
            throw new IOException("Length of key must not be longer than " + Integer.MAX_VALUE);
        }

        out.writeInt(key.length());
        out.writeChars(key);
        out.writeInt(value);
    }

    @Override
    public void read(DataInputView in) throws IOException {
        int stringLength = in.readInt();

        String key = "";
        for (int i = 0; i < stringLength; i++) {
            key += in.readChar();
        }

        Integer value = in.readInt();

        try {
            this.tuple = Tuple2.of((K) key, (V) value);
        } catch (Exception e) {
            throw new IOException();
        }
    }

    @Override
    public String toString() {
        return this.tuple.toString();
    }

}
