package de.tub.krlwlfrt;

import org.apache.flink.api.common.functions.Partitioner;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class NaiveTopK {
    public static void main(String[] args) throws Exception {
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        ParameterTool parameter = ParameterTool.fromArgs(args);

        final int k = parameter.getInt("k", 5);
        final int nodes = parameter.getInt("nodes", 9);
        final String file = parameter.get("file", "../data/data-1-0");
        final int elementsPerPartition = (int) Math.pow(26, 3);

        env.setParallelism(nodes);

        // read the data
        List<Tuple2<String, Integer>> elements = new LinkedList<Tuple2<String, Integer>>();
        Scanner s = new Scanner(new File(file));
        while (s.hasNext()) {
            String key = s.next();
            Integer value = Integer.parseInt(s.next());
            elements.add(Tuple2.of(key, value));
        }
        s.close();

        // partition the data
        DataSet<Tuple2<String, Integer>> sources = env.fromCollection(elements).partitionCustom(new Partitioner<Integer>() {
            int i = 0;

            @Override
            public int partition(Integer freq, int parallelism) {
                assert parallelism == nodes;

                return i++ / elementsPerPartition;
            }
        }, 1).sortPartition(1, Order.DESCENDING);

        DataSet<Tuple2<String, Integer>> topK = sources
                .groupBy(0)
                .sum(1)
                .setParallelism(1)
                .sortPartition(1, Order.DESCENDING)
                .first(k);

        topK.print();
    }
}
