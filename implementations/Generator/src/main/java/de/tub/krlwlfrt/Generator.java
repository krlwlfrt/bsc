package de.tub.krlwlfrt;

import org.apache.commons.math3.distribution.ParetoDistribution;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Generator {
    static List<String> generate(int nodes, int distribution) {
        List<String> elements = new LinkedList<String>();

        ParetoDistribution paretoDistribution = new ParetoDistribution();
        Random generator = new Random();

        for (int o = 0; o < nodes; o++) {
            for (int i1 = 0; i1 < 26; i1++) {
                int asciiValue = i1 + 65;
                char c1 = (char) asciiValue;

                for (int i2 = 0; i2 < 26; i2++) {
                    asciiValue = i2 + 65;
                    char c2 = (char) asciiValue;

                    for (int i3 = 0; i3 < 1; i3++) {
                        asciiValue = i3 + 65;
                        char c3 = (char) asciiValue;

                        String key = Character.toString(c1) + Character.toString(c2) + Character.toString(c3);
                        int value;

                        if (distribution == 0) {
                            value = generator.nextInt(1000);
                        } else if (distribution == 1) {
                            if (o % 2 == 0) {
                                value = generator.nextInt(1000);
                            } else {
                                value = (int) paretoDistribution.sample();
                            }
                        } else {
                            value = (int) paretoDistribution.sample();
                        }

                        elements.add(key + " " + value);
                    }
                }
            }
        }

        return elements;
    }

    public static void main(String[] args) throws Exception {
        for (int distribution = 0; distribution < 3; distribution++) {
            for (int i = 0; i < 100; i++) {
                Files.write(Paths.get("data-" + distribution + "-" + i), Generator.generate(9, distribution));
            }
        }
    }
}
