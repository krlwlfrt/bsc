package de.tub.krlwlfrt;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.io.File;
import java.util.*;

public class CachedTopK {
    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("CachedTopK");

        JavaSparkContext sc = new JavaSparkContext(conf);

        final int k = 5;
        final int nodes = 9;
        final double alpha = 1.0;

        String file = "../data/data-1-0";
        if (args.length == 2 && args[0].equals("-file")) {
            file = args[1];
        }

        // read the data
        List<Tuple2<String, Integer>> elements = new LinkedList<Tuple2<String, Integer>>();
        Scanner s = new Scanner(new File(file));
        while (s.hasNext()) {
            String key = s.next();
            Integer value = Integer.parseInt(s.next());
            elements.add(Tuple2.apply(key, value));
        }
        s.close();

        // create 4 partitions
        JavaRDD<Tuple2<String, Integer>> tuples = sc.parallelize(elements, nodes);

        // sort data in partitions and cache them
        JavaRDD<Tuple2<String, Integer>> cachedTuples = tuples
                .mapPartitions(new FlatMapFunction<Iterator<Tuple2<String, Integer>>, Tuple2<String, Integer>>() {
                    public Iterator<Tuple2<String, Integer>> call(Iterator<Tuple2<String, Integer>> tuples) throws Exception {
                        List<Tuple2<String, Integer>> tuplesList = new LinkedList<Tuple2<String, Integer>>();

                        while (tuples.hasNext()) {
                            tuplesList.add(tuples.next());
                        }

                        tuplesList.sort(new Comparator<Tuple2<String, Integer>>() {
                            public int compare(Tuple2<String, Integer> tuple1, Tuple2<String, Integer> tuple2) {
                                return tuple2._2() - tuple1._2();
                            }
                        });

                        return tuplesList.iterator();
                    }
                })
                .cache();

        ////////// PHASE ONE
        // get first k tuples from all partitions
        JavaRDD<Tuple2<String, Integer>> phaseOneCollectedTuples = cachedTuples
                .mapPartitions(new FlatMapFunction<Iterator<Tuple2<String, Integer>>, Tuple2<String, Integer>>() {
                    public Iterator<Tuple2<String, Integer>> call(Iterator<Tuple2<String, Integer>> tuples) throws Exception {
                        List<Tuple2<String, Integer>> tuplesList = new LinkedList<Tuple2<String, Integer>>();

                        int i = 0;
                        while (tuples.hasNext() && i++ < k) {
                            tuplesList.add(tuples.next());
                        }

                        return tuplesList.subList(0, 5).iterator();
                    }
                });
        // group, reduce and sort them
        JavaRDD<Tuple2<String, Integer>> phaseOneTuples = phaseOneCollectedTuples
                .groupBy(new Function<Tuple2<String, Integer>, String>() {
                    public String call(Tuple2<String, Integer> tuple) throws Exception {
                        return tuple._1();
                    }
                })
                .map(new Function<Tuple2<String, Iterable<Tuple2<String, Integer>>>, Tuple2<String, Integer>>() {
                    public Tuple2<String, Integer> call(Tuple2<String, Iterable<Tuple2<String, Integer>>> tuples) throws Exception {
                        int sum = 0;

                        String tupleList = "";

                        for (Tuple2<String, Integer> tuple : tuples._2()) {
                            sum += tuple._2();
                            tupleList += tuple + " ";
                        }

                        return Tuple2.apply(tuples._1(), sum);
                    }
                })
                .sortBy(new Function<Tuple2<String, Integer>, Integer>() {
                    public Integer call(Tuple2<String, Integer> tuple) throws Exception {
                        return tuple._2();
                    }
                }, false, 1);
        // get candidates for phase two
        List<Tuple2<String, Integer>> phaseOneCandidates = phaseOneTuples
                .collect()
                .subList(0, 5);
        // calculate phase one bottom
        final int phaseOneBottom = phaseOneCandidates
                .get(k - 1)
                ._2();
        // calculate threshold
        final int threshold = (int) (((double) phaseOneBottom / nodes) * alpha);

        ////////// PHASE TWO
        // get tuples with values greater than the threshold
        JavaRDD<Tuple2<String, Integer>> phaseTwoCollectedTuples = cachedTuples
                .mapPartitions(new FlatMapFunction<Iterator<Tuple2<String, Integer>>, Tuple2<String, Integer>>() {
                    public Iterator<Tuple2<String, Integer>> call(Iterator<Tuple2<String, Integer>> tuples) throws Exception {
                        List<Tuple2<String, Integer>> tuplesList = new LinkedList<Tuple2<String, Integer>>();

                        while (tuples.hasNext()) {
                            Tuple2<String, Integer> tuple = tuples.next();

                            if (tuple._2() >= threshold) {
                                tuplesList.add(tuple);
                            }
                        }

                        return tuplesList.iterator();
                    }
                }).cache();
        // group, reduce and sort them
        JavaRDD<Tuple2<String, Integer>> phaseTwoTuples = phaseTwoCollectedTuples
                .groupBy(new Function<Tuple2<String, Integer>, String>() {
                    public String call(Tuple2<String, Integer> tuple) throws Exception {
                        return tuple._1();
                    }
                })
                .map(new Function<Tuple2<String, Iterable<Tuple2<String, Integer>>>, Tuple2<String, Integer>>() {
                    public Tuple2<String, Integer> call(Tuple2<String, Iterable<Tuple2<String, Integer>>> tuples) throws Exception {
                        int sum = 0;

                        for (Tuple2<String, Integer> tuple : tuples._2()) {
                            sum += tuple._2();
                        }

                        return Tuple2.apply(tuples._1(), sum);
                    }
                })
                .sortBy(new Function<Tuple2<String, Integer>, Integer>() {
                    public Integer call(Tuple2<String, Integer> tuple) throws Exception {
                        return tuple._2();
                    }
                }, false, 1);
        // calculate phase two bottom
        final int phaseTwoBottom = phaseTwoTuples
                .collect()
                .subList(0, 5)
                .get(k - 1)
                ._2();
        // get top k candidates whose upper bounds are higher than the phase two bottom
        JavaRDD<Tuple2<String, Integer>> phaseTwoCandidateTuples = phaseTwoCollectedTuples
                .groupBy(new Function<Tuple2<String, Integer>, String>() {
                    public String call(Tuple2<String, Integer> tuple) throws Exception {
                        return tuple._1();
                    }
                })
                .map(new Function<Tuple2<String, Iterable<Tuple2<String, Integer>>>, Tuple2<String, Integer>>() {
                    public Tuple2<String, Integer> call(Tuple2<String, Iterable<Tuple2<String, Integer>>> tuples) throws Exception {
                        int sum = 0;

                        int i = 0;
                        for (Tuple2<String, Integer> tuple : tuples._2()) {
                            sum += tuple._2();
                            i++;
                        }

                        sum += (nodes - i) * threshold;

                        Tuple2<String, Integer> tuple = Tuple2.apply(tuples._1(), sum);

                        return tuple;
                    }
                })
                .filter(new Function<Tuple2<String, Integer>, Boolean>() {
                    public Boolean call(Tuple2<String, Integer> tuple) throws Exception {
                        return tuple._2() >= phaseTwoBottom;
                    }
                });

        // get candidates for phase two
        List<Tuple2<String, Integer>> phaseTwoCandidates = phaseTwoCandidateTuples.collect();

        final List<String> phaseTwoKeysList = new ArrayList<String>();
        for (Tuple2<String, Integer> tuple : phaseTwoCandidates) {
            phaseTwoKeysList.add(tuple._1());
        }

        assert phaseOneBottom <= phaseTwoBottom;

        ///////// PHASE THREE
        // get tuples from phaseTwoCandidates
        JavaRDD<Tuple2<String, Integer>> phaseThreeCollectedTuples = cachedTuples
                .mapPartitions(new FlatMapFunction<Iterator<Tuple2<String, Integer>>, Tuple2<String, Integer>>() {
                    public Iterator<Tuple2<String, Integer>> call(Iterator<Tuple2<String, Integer>> tuples) throws Exception {
                        List<Tuple2<String, Integer>> tuplesList = new LinkedList<Tuple2<String, Integer>>();

                        while (tuples.hasNext()) {
                            Tuple2<String, Integer> tuple = tuples.next();

                            for (String key : phaseTwoKeysList) {
                                if (tuple._1().equals(key)) {
                                    tuplesList.add(tuple);
                                }
                            }
                        }

                        return tuplesList.iterator();
                    }
                });
        // group, reduce and sort them
        List<Tuple2<String, Integer>> topK = phaseThreeCollectedTuples
                .groupBy(new Function<Tuple2<String, Integer>, String>() {
                    public String call(Tuple2<String, Integer> tuple) throws Exception {
                        return tuple._1();
                    }
                })
                .map(new Function<Tuple2<String, Iterable<Tuple2<String, Integer>>>, Tuple2<String, Integer>>() {
                    public Tuple2<String, Integer> call(Tuple2<String, Iterable<Tuple2<String, Integer>>> tuples) throws Exception {
                        int sum = 0;

                        for (Tuple2<String, Integer> tuple : tuples._2()) {
                            sum += tuple._2();
                        }

                        return Tuple2.apply(tuples._1(), sum);
                    }
                })
                .sortBy(new Function<Tuple2<String, Integer>, Integer>() {
                    public Integer call(Tuple2<String, Integer> tuple) throws Exception {
                        return tuple._2();
                    }
                }, false, 1)
                .collect()
                .subList(0, 5);

        System.out.println("\n"
                + phaseOneCollectedTuples.collect().size() + "\n"
                //+ phaseOneTuples.collect() + "\n"
                //+ phaseOneCandidates + "\n"
                //+ phaseOneBottom + "\n"
                //+ threshold + "\n"
                //+ "\n"
                + phaseTwoCollectedTuples.collect().size() + "\n"
                //+ phaseTwoTuples.collect() + "\n"
                //+ phaseTwoCandidateTuples.collect().size() + "\n"
                //+ phaseTwoCandidates + "\n"
                //+ phaseTwoKeysList + "\n"
                //+ phaseTwoBottom + "\n"
                //+ "\n"
                + phaseThreeCollectedTuples.collect().size() + "\n"
                + topK + "\n");

        try {
            Thread.sleep(600000);
            sc.stop();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}