package de.tub.krlwlfrt;

import org.apache.flink.api.common.functions.*;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DependentTasksTopK {
    public static void main(String[] args) throws Exception {
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        ParameterTool parameter = ParameterTool.fromArgs(args);

        final int k = parameter.getInt("k", 5);
        final int nodes = parameter.getInt("nodes", 9);
        final String file = parameter.get("file", "../data/data-1-0");
        final int elementsPerPartition = (int) Math.pow(26, 3);

        env.setParallelism(nodes);

        // read the data
        List<Tuple2<String, Integer>> elements = new LinkedList<Tuple2<String, Integer>>();
        Scanner s = new Scanner(new File(file));
        while (s.hasNext()) {
            String key = s.next();
            Integer value = Integer.parseInt(s.next());
            elements.add(Tuple2.of(key, value));
        }
        s.close();

        // partition the data
        DataSet<Tuple2<String, Integer>> sources = env.fromCollection(elements).partitionCustom(new Partitioner<Integer>() {
            int i = 0;

            @Override
            public int partition(Integer freq, int parallelism) {
                assert parallelism == nodes;

                return i++ / elementsPerPartition;
            }
        }, 1).sortPartition(1, Order.DESCENDING);

        // get top k from each partition
        DataSet<Tuple2<String, Integer>> topKFromPartitions = sources
                .mapPartition(new MapPartitionFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
                    @Override
                    public void mapPartition(Iterable<Tuple2<String, Integer>> tupleIterator, Collector<Tuple2<String, Integer>> out) throws Exception {
                        int i = 0;

                        for (Tuple2<String, Integer> tuple : tupleIterator) {
                            if (i++ < k) {
                                out.collect(tuple);
                            }
                        }
                    }
                });

        // calculate threshold
        final DataSet<Tuple2<String, Integer>> threshold = topKFromPartitions
                .groupBy(0)
                .reduceGroup(new GroupReduceFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
                    public void reduce(Iterable<Tuple2<String, Integer>> tupleIterator, Collector<Tuple2<String, Integer>> out) {
                        int sum = 0;
                        String key = "DUMMY";

                        for (Tuple2<String, Integer> tuple : tupleIterator) {
                            key = tuple.getField(0);
                            sum += (Integer) tuple.getField(1);
                        }

                        out.collect(Tuple2.of(key, sum));
                    }
                })
                .setParallelism(1)
                .sortPartition(1, Order.DESCENDING)
                .setParallelism(1)
                .first(k)
                .setParallelism(1)
                .sortPartition(1, Order.ASCENDING)
                .setParallelism(1)
                .first(1)
                .setParallelism(1)
                .flatMap(new FlatMapFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
                    public void flatMap(Tuple2<String, Integer> tuple, Collector<Tuple2<String, Integer>> out) {
                        for (int i = 0; i < nodes; i++) {
                            out.collect(Tuple2.of("THRESHOLD", (Integer) tuple.getField(1) / nodes));
                        }
                    }
                })
                .setParallelism(1)
                .partitionCustom(new Partitioner<Integer>() {
                    int i = 0;

                    @Override
                    public int partition(Integer freq, int parallelism) {
                        assert parallelism == nodes;

                        return i++;
                    }
                }, 1);

        DataSet<Tuple2<String, Integer>> sourcesWithThreshold = sources
                .union(threshold)
                .sortPartition(1, Order.DESCENDING);

        DataSet<Tuple2<String, Integer>> elementsAboveThreshold = sourcesWithThreshold.flatMap(new RichFlatMapFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
            int threshold;

            @Override
            public void open(Configuration parameters) {
                this.threshold = -1;
            }

            @Override
            public void flatMap(Tuple2<String, Integer> tuple, Collector<Tuple2<String, Integer>> out) throws Exception {
                if (tuple.getField(0).equals("THRESHOLD")) {
                    this.threshold = tuple.getField(1);
                    return;
                }

                if (this.threshold == -1 || (Integer) tuple.getField(1) >= this.threshold) {
                    out.collect(tuple);
                }
            }
        });

        DataSet<Tuple2<String, Integer>> topKCandidates = elementsAboveThreshold
                .groupBy(0)
                .reduceGroup(new GroupReduceFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
                    public void reduce(Iterable<Tuple2<String, Integer>> tupleIterator, Collector<Tuple2<String, Integer>> out) {
                        int sum = 0;
                        String key = "DUMMY";

                        for (Tuple2<String, Integer> tuple : tupleIterator) {
                            key = tuple.getField(0);
                            sum += (Integer) tuple.getField(1);
                        }

                        out.collect(Tuple2.of(key, sum));
                    }
                })
                .setParallelism(1)
                .sortPartition(1, Order.DESCENDING)
                .setParallelism(1)
                .first(k)
                .setParallelism(1)
                .map(new MapFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
                    @Override
                    public Tuple2<String, Integer> map(Tuple2<String, Integer> tuple) throws Exception {
                        return Tuple2.of((String) tuple.getField(0), -1);
                    }
                });

        DataSet<Tuple2<String, Integer>> topK = sources
                .union(topKCandidates)
                .setParallelism(1)
                .groupBy(0)
                .reduceGroup(new GroupReduceFunction<Tuple2<String, Integer>, Tuple2<String, Integer>>() {
                    @Override
                    public void reduce(Iterable<Tuple2<String, Integer>> iterable, Collector<Tuple2<String, Integer>> collector) throws Exception {
                        boolean hadNegativeElement = false;
                        int sum = 0;
                        String key = null;

                        for (Tuple2<String, Integer> tuple : iterable) {
                            if (key == null) {
                                key = tuple.getField(0);
                            }

                            int value = tuple.getField(1);

                            if (value == -1) {
                                hadNegativeElement = true;
                            } else {
                                sum += value;
                            }
                        }

                        if (hadNegativeElement) {
                            collector.collect(Tuple2.of(key, sum));
                        }
                    }
                })
                .setParallelism(1)
                .sortPartition(1, Order.DESCENDING);

        topK.print();
    }
}
