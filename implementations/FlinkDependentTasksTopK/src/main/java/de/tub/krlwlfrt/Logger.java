package de.tub.krlwlfrt;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Logger {
    public static boolean initialized = false;

    public static void log(String message) {
        File logFile = new File("/tmp/log");

        if (logFile.exists() && !initialized) {
            logFile.delete();
            initialized = true;
        }

        try {
            logFile.createNewFile();
        } catch (IOException e) {
            System.out.println(e);
        }

        message += "\n";

        try {
            Files.write(Paths.get("/tmp/log"), message.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
