\chapter{Implementation}
\label{implementation}

As mentioned before the goal of the implementations is to find solutions that are low in bandwidth consumption and other possible overheads, e.g. latency or redundant transformations. As mentioned before the algorithm that is the starting point for all the implementations is \propername{TPUT} (see \autoref{sec:background:flink:topk:tput}). All implementations (except the baseline) are build with this algorithm in mind -- this means that they all follow the same three phase pattern:
\begin{itemize}
  \item get by rank
  \item get by value
  \item get by key
\end{itemize}

All the \propername{Flink} implementations read their data from pregenerated files that are text only tables of key value pairs -- called tuples. The purpose of those pregenerated files is to have a bit of comparability between the different implementations when it comes to testing (see \autoref{experiments}). The files  look like the following example. 

The dataflow graphs in the sections for each implementation are for illustrative purposes only to give an overview how the algorithm works -- how the data is flowing respectively. The implementation itself is explained in more detail and supported by code snippets.

\begin{minipage}{\linewidth}
\begin{lstlisting}
...
DTA 42
DTB 20
DTC 54
...
\end{lstlisting}
\end{minipage}

Every data file has $26^3 \times 9$ lines. $26^3$ is the number of all combinations from AAA to ZZZ. Every one of these keys appears 9 times per file so that every node can have their own complete set of $26^3$ tuples -- the testing cluster has 9 task managers (see \autoref{experiments}). The programs iterate over the lines of the file and read the data into \lstinline|Tuple2| objects.

\begin{minipage}{\linewidth}
\begin{lstlisting}
Scanner s = new Scanner(new File(file));
while (s.hasNext()) {
    /* ... */
    elements.add(Tuple2.of(key, value));
}
\end{lstlisting}
\end{minipage}

\begin{wrapfigure}{r}{0.5\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{img/source-partition-sort}
  \end{center}
  \vspace{-20pt}
  \caption[Source, partition, sort]{Source, partition, sort -- First steps of all \propername{Flink} implementations}
  \vspace{-10pt}
\end{wrapfigure}

To make sure that the data is distributed correctly, it is explicitly partitioned. In this use case this is a little tedious in \propername{Flink}, because one has to specify exactly how the partitioning shall be done -- but this can be very useful in other use cases. Basically the \propername{Java} \lstinline|List| is transformed into a \propername{Flink} \lstinline|Dataset| and then partitioned after every $26^3$th element so that the result are 9 equally sized partitions that are also already sorted by the values of the tuples.\\
Now every implementation in \propername{Flink} has the same starting point for their calculations. A partitioned \lstinline|DataSet| consisting of $26^3 \times 9$ tuples. These partitions could be thought of different parts in a \propername{HDFS} storage which would translate into \lstinline|DataSet| partitions and are therefore for the purposes of these implementations equal to distributed storage.\\
The code that achieves that in \propername{Flink} looks like follows:

\begin{minipage}{\linewidth}
\begin{lstlisting}
final int elementsPerPartition = (int) Math.pow(26, 3);
DataSet<Tuple2<String, Integer>> sources = env.fromCollection(elements).partitionCustom(new Partitioner<Integer>() {
    int i = 0;

    @Override
    public int partition(Integer freq, int parallelism) {
        assert parallelism == nodes;

        return i++ / elementsPerPartition;
    }
}, 1).sortPartition(1, Order.DESCENDING);
\end{lstlisting}
\end{minipage}

\section{Baseline: Naive implementation}

\begin{figure}[ht]
  \centering
  \includegraphics[width=1.0\textwidth]{img/FlinkNaiveTopK}
  \caption[Naive Top-K execution plan]{Naive Top-K execution plan -- screenshot from \propername{Flink} web interface}
  \label{fig:implementation:naive}
\end{figure}

The baseline implementation is rather straightforward as is visible in the above dataflow -- it is basically a straight line. The implementation fits into 6 easy to comprehend lines -- thanks to the mostly expressive API of \propername{Flink}.

\subsection{Description \& decisions}

\begin{minipage}{\linewidth}
\begin{lstlisting}
DataSet<Tuple2<String, Integer>> topK = sources
    .groupBy(0)
    .sum(1)
    .setParallelism(1)
    .sortPartition(1, Order.DESCENDING)
    .first(k);
\end{lstlisting}
\end{minipage}

First all tuples are grouped by their first field (which is the key). Then all the second fields inside the groups are summed up (which are the values) so that the result is a \lstinline|Dataset| that contains $26^3$ tuples. Now it is important to tell \propername{Flink} to execute the following with only a parallelisation of 1, because otherwise it would sort on 9 different partitions which yielded non-deterministic results in my tests. Lastly it takes the first $k$ tuples from the \lstinline|DataSet| which now form the Top-K tuples.

\subsection{Problems}

The obvious problem of the implementation is that all data needs to be transferred and that it can not be parallelized. In the test cases these were $26^3 \times 9$ elements that get reduced to $26^3$ after the second step as mentioned before which is still a huge number. The number of elements could of course be arbitrarily huge and would potentially mean a substantial overhead. Luckily \propername{Flink} handles such data quite efficiently (see \autoref{sec:background:flink:dataflow-model}) but it is definitely a violation of the goal to implement solutions that are low in overhead.\\
Sorting $26^3$ elements is also very expensive -- especially since it must be done on one node only to yield the correct result. Again the number of elements could potentially by arbitrarily huge which would make the sorting even more expensive.

So this implementation is very bad from a theoretical viewpoint because it does not scale and gets more and more expensive and time consuming the more elements are put into it.

\FloatBarrier
\section{Iterative implementation}

The dataflow of the iterative implementation is a little more complex. The dataflow is split and then united again which is the only way to build sequential operations in \propername{Flink}.\\ Unfortunately it is not possible to explicitly wait for the result of one computation and incorporate this into the next one. \propername{Flink} treats unconnected sources as separate programs and deploys them accordingly and values that one would expect to be filled by the first computation are empty, null or $0$ (their initial value in the program) for the second computation even if it is computed after the first one because the closures of the programs are not connected once they are deployed and no communication happens between them afterwards.\\
As this implementation follows the \propername{TPUT} scheme and squeezes that into an iterative process it has to abuse the framework a little bit. It uses aggregators to communicate data between the partitions (see \autoref{sec:background:flink:iterations-aggregators}).

\begin{figure}[ht]
  \centering
  \includegraphics[width=1.0\textwidth]{img/FlinkIterativeTopK}
  \caption[Iterative Top-K execution plan]{Iterative Top-K execution plan -- screenshot from web interface}
\end{figure}

\subsection{Description \& decisions}

First it turns the \lstinline|DataSet| into an \lstinline|IterativeDataSet| which is simply an abstraction \propername{Flink} to start the iteration. The upper bound for the number of iterations is set to \lstinline|elementsPerPartition| which is actually too much -- by factor $k$ -- because per iteration $k$ elements are communicated per partition.

\begin{minipage}{\linewidth}
\begin{lstlisting}
IterativeDataSet<Tuple2<String, Integer>> it = sources.iterate(elementsPerPartition);
\end{lstlisting}
\end{minipage}

\subsubsection{Iteration: Aggregator \& convergence criterion}

\begin{wrapfigure}{r}{0.5\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{img/FlinkIterativeTopK-iteration}
  \end{center}
  \vspace{-20pt}
  \caption{Iteration part in iterative implementation}
  \vspace{-10pt}
\end{wrapfigure}

A convergence criterion and an aggregator are added for the iteration. Unfortunately \propername{Flink} does not only deploy one instance of the class but at least 4. The one that aggregates the total aggregate is not reachable directly from the worker nodes so it has to be distinguished by telling the reachable aggregators that they are not the ``global'' via a property.

\begin{minipage}{\linewidth}
\begin{lstlisting}
public void setGlobal(boolean global) {
   this.global = global;
}
\end{lstlisting}
\end{minipage}

The interface for aggregators has 3 methods. \lstinline|getAggregate| to get the aggregated value from the aggregator -- called at the end of each iteration, \lstinline|aggregate| to add values to the aggregate and \lstinline|reset| to reset the aggregate -- called at the beginning of each iteration. The aggregator instances that are not the global one are reset after each iteration -- the global is not to keep the yet aggregated list of elements.\\

\begin{minipage}{\linewidth}
\begin{lstlisting}
@Override
public void reset() {
    if (!this.global) {
        this.aggregatedList.getList().clear();
    }
}
\end{lstlisting}
\end{minipage}

The aggregator is implemented as a dedicated class. It aggregates tuples from the nodes that are part of the iteration task -- $k$ per iteration. The first $k$ elements equal the first round of \propername{TPUT} and the aggregator computes the threshold from them.

\begin{minipage}{\linewidth}
\begin{lstlisting}
if (threshold == 0) {
    threshold = deduplicatedList.get(k - 1).f1 / nodes;
}
\end{lstlisting}
\end{minipage}

After each iteration the aggregator deduplicates the list, aggregating tuples with the same key and computing the partial sums of the values. The data should actually be kept in a different structure to be able to prune away ineligible elements -- at the end of phase two -- of which the upper bounds are below the threshold but because this would have meant a far more complex class and it does not concern the main problem of the implementation it was implemented as a single list of partial sums (see \autoref{sec:implementation:iterative:problems}).

The aggregator considers the iteration converged when one worker node aggregates a tuple with a value that is below the threshold\footnote{Actually this should be done when the aggregator recieves only tuples with values below the threshold in one iteration but this way it was implementationally easier and since it is not the real problem of this approach it is sufficient for the purpose of this thesis.}.

\begin{minipage}{\linewidth}
\begin{lstlisting}
if (value < threshold) {
    this.converged = true;
}
\end{lstlisting}
\end{minipage}

Actually the aggregator should not be doing this at all because that is what the convergence criterion is for but it would be impossible to check this in the convergence criterion cause it never sees individual elements but always a list of partial sums.

When the aggregator considers the iteration as converged it puts a dummy element at the beginning of the list which has the key \lstinline|"CONVERGED"| to signal the convergence criterion and the workers that the iteration should finish.

\begin{minipage}{\linewidth}
\begin{lstlisting}
if (this.converged) {
    this.aggregatedList.add(new Tuple2Value(Tuple2.of("CONVERGED", 0)));
}
\end{lstlisting}
\end{minipage}

When the convergence criterion encounters this dummy element it schedules itself with a property for converging after the next iteration to give the worker nodes one more iteration to get the elements from the aggregation to succeed with the computation and then signal the iteration synchronization to finish the iteration.

The values that are shall be aggregated must be serializable by \propername{Flink} as they are possibly send over the wire (see \autoref{sec:background:flink:serialization}). \propername{Flink} offers classes to encapsulate primitive data types like \lstinline|int| or \lstinline|boolean| to achieve the serialization. For more complex data types it is up to the developer to implement such encapsulating classes.\\
\propername{Flink} offers an interface for that purpose that must be implemented which is simply called \lstinline|Value|. The iterative implementation needed a class for serializable \lstinline|Tuple2|s and one for lists of those. They are basically implemented by writing to or reading from a stream of data which is pretty straightforward.

\subsubsection{Worker nodes}

The worker nodes execute a flat map. A flat map is not bound to return the same number of elements that went into it. It can be 0, less, exactly the same or more. The workers aggregate a moving window of $k$ elements per iteration. $[0,k-1]$ in the first iteration, $[k,2k-1]$ in the second and so forth. These are handled by the aggregators as described before. Furthermore they feedback the tuples to the dataset so the standing loop can aggregate the following window in the next iteration.

\begin{minipage}{\linewidth}
\begin{lstlisting}
if ((countElements >= (iteration) * elementsPerIteration) && (countElements < (iteration + 1) * elementsPerIteration)) {
    tuple2ListAggregator.aggregate(tuple2Value);
}

this.countElements++;
out.collect(tuple);
\end{lstlisting}
\end{minipage}

At the beginning of each iteration the worker nodes request the aggregate from the last iteration. When the worker nodes encounter the dummy element in the list of aggregated elements they fetch elements from that list and feed them back to the dataset instead of the tuple that they are supposed to map. So that the result of the iteration is a new \lstinline|DataSet| with k elements whose values are all set to $-1$ do distinguish them from real values after merging the dataflow again.

\begin{minipage}{\linewidth}
\begin{lstlisting}
if (converged) {
    if (possibleTopK.getList().size() > 1) {
        Tuple2<String, Integer> tupleToCollect = possibleTopK.getList().remove(1).getValue();
        if (!tupleToCollect.getField(0).equals("CONVERGED")) {
            out.collect(Tuple2.of((String) tupleToCollect.getField(0), -1));
        }
        return;
    }
    return;
}
\end{lstlisting}
\end{minipage}

\subsubsection{Merging the dataflow}

\begin{wrapfigure}{r}{0.5\textwidth}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{img/FlinkIterativeTopK-union}
  \end{center}
  \vspace{-20pt}
  \caption{Merging the dataflow and calculating the \propername{Top-K}}
  \vspace{-10pt}
\end{wrapfigure}

The candidates that were produced in the iteration are now merged into the original source (see \autoref{sec:implementation:iterative:problems}). In the resulting unified \lstinline|DataSet| all elements with the same key are grouped together. Groups that contain an element with a negative value are in the \propername{Top-K} elements and therefore their values are summed up and they end up in the result set that is only once more sorted descending by values. So the result is a sorted list of the \propername{Top-K} elements -- or at least mostly because of the afore mentioned short comings of the implementation.

\begin{lstlisting}
boolean hadNegativeElement = false;
int sum = 0;
String key = null;

for (Tuple2<String, Integer> tuple : iterable) {
    if (key == null) {
        key = tuple.getField(0);
    }
    int value = tuple.getField(1);
    if (value == -1) {
        hadNegativeElement = true;
    } else {
        sum += value;
    }
}

if (hadNegativeElement) {
    collector.collect(Tuple2.of(key, sum));
}
\end{lstlisting}

\subsection{Problems}
\label{sec:implementation:iterative:problems}

Like the previous implementation the iterative one has the same fundamental problem. It has to communicate an arbitrarily large set of data multiple times. Even if \propername{Flink}'s deployment strategy is able to mitigate this by using nodes that are geographically or latency-wise close to the original data source at the very latest when merging the dataflow again the problem occurs because \lstinline|DataSet|s are immutable. The source has to be sent somewhere else unified with the candidates and then be handled as a whole to extract the \propername{Top-K} elements. This is also why the few short comings of the implementation are irrelevant even though it is not guaranteed that it returns the real \propername{Top-K}. The fundamental problem of the approach is obvious in the fact that the whole set of data needs to be handled as a whole at one point in the implementation.

\FloatBarrier
\section{Dependent tasks}
\label{sec:implementation:dependent}

The dataflow of this implementation reveals at a glance the same problem that the previous one has. The original source has to be forwarded -- in this case 2 times. The main idea of the implementation is to compute every phase of \propername{TPUT} separately and use the result in the following computations.

\begin{figure}[ht]
  \centering
  \includegraphics[width=1.0\textwidth]{img/FlinkDependentTasksTopK}
  \caption[Dependent tasks Top-K execution plan]{Dependent tasks Top-K execution plan -- screenshot from web interface}
  \label{fig:implementation:dependent}
\end{figure}

\subsection{Description, decisions \& problems}

In the lower path of the graph the threshold is calculated by grouping the elements by their keys, summing the values in the groups, taking the $k$th element, duplicating it $n$\footnote{$n$ is the number of partitions.} times with the dummy key \lstinline|"THRESHOLD"| and creating $n$ one element partitions.

\begin{lstlisting}
for (int i = 0; i < nodes; i++) {
     out.collect(Tuple2.of("THRESHOLD", (Integer) tuple.getField(1) / nodes));
}
/* ... */
public int partition(Integer freq, int parallelism) {
    assert parallelism == nodes;

    return i++;
}
\end{lstlisting}

These partitions are then unified with the original source so that every of the original partitions has one threshold element in them -- \propername{Flink} unifies partitions of both sets one to one. This results in the middle path of the graph. The partitions are sorted and then all elements that are ``above'' the threshold element are collected for a new \lstinline|DataSet|.

\begin{lstlisting}
if (this.threshold == -1 || (Integer) tuple.getField(1) >= this.threshold) {
    out.collect(tuple);
}
\end{lstlisting}

The resulting elements are again grouped by key, sorted by value and filtered to the $k$ highest elements\footnote{This is the same short coming as the previous implementation has in regard to \propername{TPUT} but again it is not relevant to be implemented correctly in order to proof why the approach itself is flawed. Also it would have entailed a third merging of the dataflow which would make the implementation even more complex.}. The values of these ``candidates'' are set to $-1$ like in the previous implementation to distinguish them from real elements.\\
The ``candidates'' with values of $-1$ are then merged into the original source to extract the \propername{Top-K} from it -- resulting in the upper path of the graph. This again works similar to the previous implementation. The elements are grouped by keys and the groups that contain negative values are collected. This also again is the same fundamental problem like with the previous implementation. The whole dataset has to be handled at once and has to be sent over the wire for that. This violates the goal of the implementations to have the least overhead possible and especially not being forced to gather and handle all the data at once.

\FloatBarrier
\section{Apache Spark: Cached RDD}

The implementation in \propername{Spark} also reads from the same test files like the \propername{Flink} implementations to have at least some sort of comparability and because it resembles distributed storage as was explained before. The data is also partitioned into $9$ equally sized partitions and sorted descending by value inside these partitions -- which is a little unintuitive to achieve because \propername{Spark} does not provide a dedicated method for that and one has to iterate over the set manually to order it. The resulting ordered partitions are then cached in place (see \autoref{sec:background:spark}).

\begin{lstlisting}
JavaRDD<Tuple2<String, Integer>> cachedTuples = tuples
    .mapPartitions(new FlatMapFunction<Iterator<Tuple2<String, Integer>>, Tuple2<String, Integer>>() {
        public Iterator<Tuple2<String, Integer>> call(Iterator<Tuple2<String, Integer>> tuples) throws Exception {
            /** sort the partition*/
        }
    })
    .cache();
\end{lstlisting}

This is the one major difference to \propername{Flink} that it is possible to explicitly mark datasets as relevant for later usage. This cached partitioned dataset is the starting point for all the relevant computations that are needed to implement \propername{TPUT} as it resembles the group of slaves (see \autoref{sec:background:flink:topk:tput}) and the \propername{Spark} program itself resembles the master that fetches data from the slaves.

\begin{figure}[ht]
  \centering
  \includegraphics[width=1.0\textwidth]{img/CachedTopK}
  \caption[Cached Top-K]{Cached Top-K (own image) -- this is actually a depiction how the implementation works but also shows the phases of \propername{TPUT}.}
\end{figure}

\subsection{Description \& decisions}

To make the resemblance of the implementation in \propername{Spark} and \propername{TPUT} more clear the dataset transformations are listed in three groups that represent the three phases of \propername{TPUT}:

\begin{itemize}
  \item From all cached partitions the first $k$ elements are fetched, these are then grouped by key and the values of the groups are summed up -- partial sums in \propername{TPUT}. The threshold is then calculated by taking the value of the $k$th highest element and dividing it by the number of nodes -- which is also $9$ for the \propername{Spark} implementation.
  \begin{lstlisting}
List<Tuple2<String, Integer>> tuplesList = new LinkedList<Tuple2<String, Integer>>();
int i = 0;
while (tuples.hasNext() && i++ < k) {
  tuplesList.add(tuples.next());
}
return tuplesList.subList(0, 5).iterator();
  \end{lstlisting}
  \item Now all elements that are greater or equal to the computed threshold are taken from the cached partitions. These fetched elements that are not yet deduplicated are cached because they are needed again after the phase two bottom ($\tau_2$ in \propername{TPUT}) is computed. To compute the lower bound the elements are then again grouped by key and their values are summed up and sorted. Then value of the $k$th highest element is the phase two bottom.\\
  \begin{lstlisting}
final int phaseTwoBottom = phaseTwoTuples
  .collect()
  .subList(0, 5)
  .get(k - 1)
  ._2();
  \end{lstlisting}
  The fetched, not deduplicated and cached elements are now used again from cache. For each group the sum is calculated again but now the upper bounds of the elements are calculated and all by adding $(numberOfNodes - numberOfElementsInGroup) \times T$. For the elements where the value is lower than the phase two bottom are filtered out. The result is a set of \propername{Top-K} candidates.
  \item The last step now fetches all items from the partitions whose keys are in the list of candidates. These are then again grouped by key, summed up and finally the $k$ highest elements are taken which are the \propername{Top-K} elements.
\end{itemize}

The code snippet for phase two contains the method call that is the key ingredient to the sequential process of the driver program: \lstinline|collect|. The programming guide of \propername{Spark} reads:

\begin{quote}
  ``Return all the elements of the dataset as an array at the driver program. This is usually useful after a filter or other operation that returns a sufficiently small subset of the data.''\\
  \cite{Spark:programming-guide}
\end{quote}

This means that the driver program halts at this point in the program until the data is returned. When this continues the computations that were necessary to get to that point are finished and the result is computed. The result can then be used for further computations down the road.

The implementation also features the pruning parameter $\alpha$ which is used to lower the threshold $T = (\tau_1 / nodes) \times \alpha$ with $\alpha \in ]0,1[$ which leads to less overhead through irrelevant elements \cite[4,10]{Cao:2004:ETQ:1011767.1011798}.