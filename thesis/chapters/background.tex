\chapter{Background}

As this thesis is based on a good understanding of \propername{Flink} and \propername{Top-K} this chapter explains both in as much detail as needed to get a good grasp on the topic.

\section{Flink}

\propername{Flink} is an opinionated big data framework with a rather large and active community under the patronage of \propername{The Apache Software Foundation}. The core of the framework is a dataflow engine that is built on top of streams which enables parallelized computations on distributed data - from the small scale up to the cloud\cite{Flink:homepage}. \propername{Flink} originated as a joint project of the \propername{TU-Berlin}, \propername{HU-Berlin} and \propername{HPI-Potsdam} and emerged from a series of academical experiments.

\begin{quote}
``Apache Flink is an open-source system for processing streaming and batch data. Flink is built on the philosophy that many classes of data processing applications, including real-time analytics, continuous data pipelines, historic data processing (batch), and iterative algorithms (machine learning, graph analysis) can be expressed and executed as pipelined fault-tolerant dataflows. [..] Flink’s architecture [..] [unifies] a (seemingly diverse) set of use cases [..] under a single execution model.''\\
\cite[28]{IEEE-38-4-3}
\end{quote}

While traditionally batch- and stream-processing were handled as two completely different things and used different programming paradigms, \propername{Flink} handles batch-processing simply as a special case of stream-processing which allows \propername{Flink} to have the same runtime for both approaches\cite[29]{IEEE-38-4-3}. On the other hand \propername{Flink} provides a dedicated API just for batch processing cause it can work more efficiently with finite sets of data \cite[28 -- 29]{IEEE-38-4-3}.

\propername{Flink} offers an extensive API out of the box which enable a lot of different possible approaches to implement Top-K-algorithms. The dedicated API for batch processing with the existing operators \emph{map, reduce, and filter} and iterative data processing are the starting point for the attempts to implement the different approaches. \propername{Flink} also offers a dedicated API for streams of data which is not used in this thesis though because of the assumption that data that will be examined for \propername{Top-K} elements exists in static sources and is not a constant stream of data which would have needed to be transformed into batches via the windowing API of \propername{Flink} anyhow to be processable at all.

\subsection{\propername{Flink} programs \& dataflow model}

Programs in \propername{Flink} can be written in \propername{Java} or in \propername{Scala}. A program can have one or more data sources which can be dynamic (streams) or static (files, databases) data -- although not mixed in one program. Operators can be defined on these sources which transform the data -- possibly a bunch of operators chained after one another. On the end of a program one has one or more data sinks, e.g. a file or database where the transformed data is output/stored.

In a \propername{Flink} program one could for example monitor the stream of tweets on twitter in a specific time window and count the occurrences of specific words or number of tweets of authors and then write the counts to a database. Or one could gather temperatures from a set of stations, calculate the average temperature, and then feed this to a display.

\subsubsection{Dataflow model}
\label{sec:background:flink:dataflow-model}

\begin{figure}[ht]
  \centering
  \includegraphics[width=1.0\textwidth]{img/dataflow-2}
  \caption[A dataflow example]{A dataflow example\cite{Flink:concepts}}
  \label{fig:dataflow}
\end{figure}

Programs in \textsc{Flink} are rather descriptive than explicit and follow a dataflow model\cite[30]{IEEE-38-4-3}. This means that a program describes how data is ``flowing'' through a series of ``steps'' (see \autoref{sec:background:flink:operators}) while being transformed. This dataflow can be automatically analyzed and optimized by \propername{Flink} to be deployed into different execution environments (see \autoref{sec:background:flink:execution_environment}). The dataflow is a directed acyclic graph\cite[30]{IEEE-38-4-3} where the edges are the flowing data and the vertices are the transformation of the data. One could see every vertex as a set of a input, transformation and an output, the output being the input for the next vertex, and then chaining these vertices together. This modular approach is one of the powerful ideas behind the dataflow model. The above figure (see \autoref{fig:dataflow}) shows an example dataflow with a parallelism of $2$. It performs a map operation on a source and then reduces the mapped data through several steps with the \propername{Flink} API.\\
The dataflow model in \propername{Flink} is implemented in a way that is useful for data processing, e.g.: it has high but controllable throughput with low latency and fault tolerance through checkpointing \cite[30 -- 33]{IEEE-38-4-3}. This enables programs that are fast but also reliable cause errors are recoverable through snapshots of succeeding states of the program.

\FloatBarrier

Although the dataflow model has a lot of advantages built-in that make it easy to build a robust data transformation it is also a restrictive framework when implementing algorithms. Programmers have to adapt and get used to this specific approach. There are also inherent problems with the dataflow model.\\
The state of multiple/parallel instances of one step can not be shared with the other instances of the same step so that every one of those can only ever work on the portion of data that they are assigned. If one needs metrics of the complete data one has to avoid parallelism or compute something in advance and then incorporate this into the data\footnote{There have been attempts to add a parameter server to \propername{Flink}. See \url{http://apache-flink-mailing-list-archive.1008284.n3.nabble.com/Parameter-Server-for-Flink-td7423.html}, Accessed: 2016-11-28}.\\
There is no possibility to feed data back into the system -- the data is only transformed forwards so that one can not transform a portion of the data and then incorporate an earlier state of the same data from the transformed one. An exception to that in \propername{Flink} is the support for iterations where one can iterate over the same data again and again, gathering up information or adjusting parameters which is especially useful for applications such as machine learning.\\
Every operator can only have one output which is transformed input data. One can not additionally output anything else which is not in the transformed data. That makes it hard to compute metrics about the data for later usage. In this sense it is functional programming cause there are no side effects if only input and output exist -- the exception to that are iterations and aggregators (see \autoref{sec:background:flink:iterations-aggregators}). This makes it easier to reason about implementations but makes it harder to implement some algorithms which will become obvious later.

\subsection{Execution environment}
\label{sec:background:flink:execution_environment}

Since \propername{Flink} runs on the JVM (as it is written in \propername{Java} and \propername{Scala} respectively) it can be executed on any operating system that is compatible to \propername{Java}. \propername{Flink} is very versatile when it comes to execution environments:

\begin{itemize}
	\item A local JVM, either started dynamically or on a dedicated instance of Flink running locally\cite{Flink:setup:local}
	
	\item A cluster of \propername{Flink} nodes onto which several steps of the dataflow are deployed\cite{Flink:setup:standalone-cluster} either controlled through \propername{Apache YARN}\cite{Flink:setup:yarn} or manually\footnote{For example \propername{Flink} can be deployed to \propername{AWS}\cite{Flink:setup:aws} \& \propername{Google Computing Engine}\cite{Flink:setup:google-compute-engine}.}
\end{itemize}

This diversity of options to deploy a program is a big advantage of the framework which is partially due to its dataflow model that makes the programs flexible for scaling -- the parallelism of steps is mostly limited by hardware. Also it makes development easier cause a developer can test programs locally before scaling them up in a cloud environment for bigger amounts of data.

\subsection{Operators}
\label{sec:background:flink:operators}

As mentioned before \propername{Flink} offers two different APIs -- one for streaming and one for batch data. These APIs both offer an extensive set of methods to transform data -- this section will introduce the most relevant and most used in a short overview.

\subsubsection{Map, Reduce \& Filter (and other operators)}

As a \propername{Flink} program works on sets of items one needs methods like
\begin{itemize}
  \item \propername{map} a transformation to all of them,
  \item \propername{reduce} them all to one single item or
  \item \propername{filter} items that match certain criteria.
\end{itemize}
These three are the most common methods one would use in a \propername{Flink} program or for that matter in any \propername{Big Data} framework.

A common use case for \propername{map} would be, e.g. to convert the price of a list of products to another currency or compute the temperature in degree Celsius from given temperatures in degree Fahrenheit for a set of measurements.\\
\propername{Reduce} can for example be used to calculate the average of a value in a set of items, e.g. the average temperature between a set of thermometers by gradually adding them all up and dividing them by the overall amount.\\
\propername{Filter} could be useful to select only temperature measurements from a specific area or within margin of values that are interesting.

\propername{Flink} offers a great variety of other operators to transform data. These range from specific cases of the mentioned map, reduce and filter -- e.g. a function to filter only the first $n$ elements of a set -- to methods to join sets of data with other sets or partition them in smaller sets for further more specific treatments.

\subsubsection{Extensibility}

One useful feature of the operators described in the previous section is the extensibility of said operators. One is completely free in specifying what one wants to do with the data by implementing the specifics of what an operator should do, which \propername{Flink} then simply executes -- so there are no real boundaries to what one can do with a set of data. A custom ``mapper'' or ``reducer'' (and so forth) is as simple as just a dedicated class that implements a specific interface which also makes the code reusable in the scope of the types that the class applies to, namely: what goes into the transformation and what it is transformed to.

\subsubsection{Parallelisation}

All operators in \propername{Flink} that handle items in a set of data independently can be parallelized. Depending on the degree of parallelisation this can speed up the operator and in turn the program immensely especially for computationally expensive tasks. The deployment of an operator is transparently handled by \propername{Flink} for the developer. Whether the ``mapper'' runs only on one node or concurrently on multiple nodes is up to \propername{Flink}'s deployment decisions. It is possible to specifically set the degree of parallelism specific for each operator but that is only necessary in development or specific use cases because \propername{Flink} can usually extrapolate the best deployment strategy depending on the data and the setting it will run on.\\
\propername{Flink} does not only rely on the analysis of the dataflow itself but also analyses the user defined functions (UDF) to infer types and modified values to make better decisions in regard to execution of the dataflow and the UDF\footnote{See \url{https://issues.apache.org/jira/browse/FLINK-1319}}.

\subsection{Sources \& Sinks}

Sources and sinks can be understood as they are used in graph theory. They either provide data to the dataflow or consume it. As mentioned before (see \autoref{sec:background:flink:dataflow-model}) one could understand every vertex of the dataflow graph as a \propername{Flink} program of its own with a source and sink, but if vertices are chained up with sinks to sources until one connected all vertices together there will ``normally'' be only one or more sources at the beginning of the dataflow and one or more sinks at the end -- of course one could have more sources feeding the dataflow or sinks consuming data in between.\\
Also as mentioned before sources and sinks could be simply a file or the output of the \propername{Flink} instance the program is run on. Sources and sinks could be a variety of other systems like databases, a twitter stream, cloud storage or even \propername{Elasticsearch}. For some of those \propername{Flink} even provides support out of the box\cite{Flink:application-development:connectors:overview}, but again \propername{Flink} offers ways to build own connectors to use other sources and sinks.

\subsection{Iterations \& Aggregators}
\label{sec:background:flink:iterations-aggregators}

As mentioned before (see \autoref{sec:background:flink:dataflow-model}) the only intended way to break free of the dataflow model and feedback data in \propername{Flink} are iterations. This only works on sets of data and not on streams. One defines a set of data to iterate on. This set is then distributed to worker nodes and iterated on until an upper bound of iterations is reached or an optional convergence criterion is met.\\
The user can optionally define one or more aggregators which are simply connection end points where the workers can send data to (usually aggregate them in some manner -- like summing them up). Since there can be more than one worker nodes these aggregators are potentially on other machines and so the user has to keep in mind that the data must be send over the wire. The aggregated value of the last iteration can then be used in the next iteration to transform the data in the worker nodes.\\
The convergence criterion is again an UDF in which the developer can check the aggregated value of the last iteration and then decide whether or not the set is converged. This will stop the iteration process immediately and return the data that is still on the workers as the transformed set of data.

As mentioned before iterations and aggregators are especially useful in use cases like machine learning or evolutionary algorithms where one wants to find a parameter through gradient decent or similar applications. They also allow to work on portions of sets individually and distributed. A key point to the usefulness of iterations is that the data is distributed only once to the worker nodes and then reused throughout the iterations. The data remains on the nodes and can be transformed over and over in each iteration so there is no overhead for redistributing the data.

\subsection{Serialization}
\label{sec:background:flink:serialization}

As \propername{Flink} can be deployed in different environments (see \autoref{sec:background:flink:execution_environment}) it needs a unified way to exchange data between steps of the dataflow. The program itself is deployed as a JAR file but the data that one operator produces must be transferred over the wire to be consumed by the next operator in line. As data is held inside of Java objects (instantiated classes) and it is not possible to simply transfer these objects they need to be represented differently to transfer them. This is were serialization is used in \propername{Flink}. The state of the object -- essentially the data it contains -- is transformed into a string of bytes, transferred to the next step of the dataflow and then a new object is constructed by deserializing the byte string. So basically one destroys the objects in one step and builds new ones in the next step.\\
Serialization is necessary to be able to store or transfer the data at all. When one wants to use a datatype not built in to \propername{Flink} that can not be automatically serialized one has to handle the serialization manually, precisely telling \propername{Flink} what the type of the data is or explicitly how to serialize the data cause the built-in serializers might fail on complex data types.\cite{Flink:application-development:data-types-and-serialization}

\subsection{Community}

\propername{Flink} has a rather large community for an open source project especially keeping in mind that its origins are in the academic context. \propername{GitHub} states that the project has 252 contributors\footnote{As of 2016-11-23, see \url{https://github.com/apache/flink}} with dozens of new commits every day and people that are not members of the project offering new features via pull requests and reporting bugs. The project itself offers mailing lists and other channels to communicate and get involved making it easy to participate and contribute. Also the project offered two conferences so far\footnote{See \url{http://flink-forward.org/}} and serveral meetups where developers and users can meet and discuss.\\
\propername{Flink} is a top level project in \propername{The Apache Software Foundation} since January 2015 and is backed by a Berlin-based company called \propername{dataArtisans}\footnote{See \url{http://data-artisans.com/} for further information} which both implies a solid foundation for the project so that users can rely on the availability and stability of \propername{Flink} for at least a few years to come.\\
\propername{Flink} is used in big companies like \propername{Ericsson} or \propername{Zalando}\footnote{See \url{https://flink.apache.org/poweredby.html} for other adopters} which shows that it is adoptable in the ``real'' world and not only for research purposes.

\subsection{Other ``big data'' frameworks}

There are a lot of ``big data'' frameworks to choose from. Some of the biggest are \propername{Hadoop}, \propername{Spark} and \propername{Storm}\footnote{The Apache prefix is left out for brevity and better readability.}.\\
\propername{Hadoop} originated from the \propername{MapReduce} algorithm of \propername{Google} and has a vast ecosystem of tools, interfaces and other frameworks like Hadoop File System (\propername{HDFS}), \propername{YARN}, \propername{Hive} and \propername{Pig}, and \propername{Spark} and \propername{Flink}. Hadoop is still used by a large number of projects and organizations\footnote{See \url{https://wiki.apache.org/hadoop/PoweredBy}} although it is rather old and does not have the new concepts like \propername{Flink}. Tools like \propername{HDFS} and \propername{YARN} are essential to a lot of other big data frameworks as storage and orchestration respectively.\\

Most frameworks are opinionated and built on specific philosophy how to store, transform and fetch data. Like \propername{Flink} is built centered around a streaming engine atop a dataflow model other frameworks do not have this kind of foundation or a totally different one -- like \propername{Spark}.

\section{Apache Spark}
\label{sec:background:spark}

\propername{Apache Spark}\footnote{From here on out called \propername{Spark} for brevity} and \propername{Flink} have a lot of the things in common that were explained about \propername{Flink} before -- besides the same languages (\propername{Java} and \propername{Scala}). But they also differ quite substantially in two main points which define what is possible with both of them:

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.75\textwidth]{img/spark-program-structure}
	\caption[Spark program scheme]{Spark program scheme \cite{Spark:cluster-mode-overview}}
\end{figure}

The foundation of \propername{Spark} are resilient distributed datasets (RDDs). They are resilient in a sense that they can be rebuilt if one partition is lost. They can be cached in their distributed state -- this means that they can be held in memory for faster querying\cite[1]{Zaharia:2010:SCC:1863103.1863113}. Although this is just a ``recommendation'' for the framework and \propername{Spark} can decide to ignore it -- or has to ignore it because it runs out of memory on a machine -- this is a very powerful feature to keep data cached in a permanent location\cite[2]{Zaharia:2010:SCC:1863103.1863113}. This feature is something that \propername{Flink} can not offer in such a manner\cite[3]{Zaharia:2010:SCC:1863103.1863113}.\\
This technique can yield immense performance boosts after a warm-up phase. Computation and caching of RDDs is lazy. This means that they are only executed upon usage. The first access can then be rather slow but subsequent accesses are much faster\cite[3, 5]{Zaharia:2010:SCC:1863103.1863113}.\\
The downside of RDDs is that the framework is limited in what one can do with it and other use cases have to be built atop or rather be squeezed into these RDDs. \propername{Spark} is not suited for stream processing as much as \propername{Flink} is because of that.

The other difference is that it is not based on a dataflow model because it is meant for other applications than \propername{Flink}\cite[1]{Zaharia:2010:SCC:1863103.1863113}. On the one hand that means that \propername{Spark} lacks the advantages that the dataflow model offers (see \autoref{sec:background:flink:dataflow-model}) or has to implement them in a different way.\\
On the other hand it enables completely different applications where one does not need to express a complete program in a dataflow. This is especially useful if one needs to compute something off of a set of data that is needed to compute something else subsequentially on the same data. This makes it possible to build sequences of computations that can also profit from cached RDDs.

\section{Top-K}

\propername{Top-K} is a class of algorithms which have in common that they determine the $k$ best, most, highest, worst, least, lowest, etc. items in a set of items. As mentioned before in this thesis particularly distributed variants of this algorithms are of relevance -- where one wants to get the \propername{Top-K} elements from a set of elements that is distributed over several nodes with overlapping subsets.\\
The main intention of these distributed variants of the algorithm are to compute the \propername{Top-K} without ever seeing all elements from all subsets cause these could be very large. There are different ways to achieve that:

One group of algorithms aims to compute the real \propername{Top-K} of a distributed set. Some of them use a threshold heuristic to fetch elements that could be candidates for the real \propername{Top-K} -- \propername{TPUT} (which is relevant in this thesis) falls into that category. These algorithms have the disadvantage that they potentially have a high overhead and communicate a lot of unimportant items that are not in the real \propername{Top-K} (see \autoref{sec:background:flink:topk:tput}).

The other group of algorithms is probabilistic and does not compute the real \propername{Top-K} but a sufficient estimate. In some use cases it is only necessary to compute bounds for items instead of computing their real values\cite{Fagin:2001:OAA:375551.375567}. Statistical models make it possible to estimate values of items for subsets of the distributed set that have not been queried and thus making it unnecessary to get items from all subsets\cite{Michel:2005:KFD:1083592.1083667}. The disadvantage of those algorithms is that they rely on complex computations and do not result in the real \propername{Top-K} -- which suffices in some use cases.

\subsection{TPUT -- Three-Phase Uniform Threshold}
\label{sec:background:flink:topk:tput}

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\textwidth]{img/master-slave-system}
  \caption[Master slave system example]{Master slave system example (own image) -- flags from \url{http://www.famfamfam.com/lab/icons/flags/}}
  \label{fig:tput}
\end{figure}

\propername{TPUT} assumes that a distributed system exists with a master node and an arbitrary number of slaves that hold the subsets of the data of which one wants to compute the \propername{Top-K} elements of. As mentioned before these slave nodes can be geographically rather far apart -- because of that it matters to reduce overheads.\\
As the name suggests the algorithm consists of three phases -- or steps:

\begin{itemize}
  \item In the first step all slave nodes send their \propername{Top-K} items to the master (compare \autoref{fig:tput}). The master then calculates the partial sums of the elements. The value of the $k$th highest partial sum is the lower bound of the first phase -- called $\tau_1$. The threshold $T$ is computed by dividing $\tau_1$ through the number of slave nodes $m$ -- $T = \tau_1 / m$.
  \item The master asks all slave nodes for their elements with values higher than $T$. It then again calculates the partial sums for all received elements and sets the lower bound $\tau_2$ to be the $k$th highest partial sum. Upper bounds for the elements are computed by setting not yet seen values to $T$. Elements with upper bounds lower than $\tau_2$ are eliminated. The remaining elements are the candidates for the \propername{Top-K}.
  \item Finally the master asks all slave nodes for their values for the candidates that have been computed in the previous step. From these values the real \propername{Top-K} can be calculated.\\
  (see \cite[3]{Cao:2004:ETQ:1011767.1011798} for further details)
\end{itemize}
  
The advantage of \propername{TPUT} is that it terminates after three round trips -- guaranteed. As mentioned before the disadvantage of \propername{TPUT} is that it is possible that a lot of redundant data is sent -- especially for odd distributions of data in the subsets. A slave node could have a very long list of elements with values higher then the threshold\cite[4]{Cao:2004:ETQ:1011767.1011798}. There are several ways to mitigate this\cite[Ibid]{Cao:2004:ETQ:1011767.1011798} but since it is hard enough to implement the basic algorithm in the dataflow model they will not be used for the \propername{Flink} implementations. But this is discussed in more detail in the following chapter.
