\chapter{Experiments}
\label{experiments}

\section{Flink algorithms}

Although the implementations do not yield the correct results (implementational short comings) and do not achieve the proposed goals (small overheads) they were tested on a \propername{Flink} cluster to evaluate their performance. The test data consisted of 91 files where the values for the keys were uniformly distributed. 91 contain $^5/_9$ uniformly distributed and $^4/_9$ Zipf distributed files\footnote{Zipf is the discrete version of the Pareto distribution. Because the data was generated with a Java-Class that generates values of a Pareto distribution the measurements refer to \textit{pareto} instead of Zipf}. Another 91 files contain only Zipf distributed values. All the files contained $26^3 \times 9$ elements as was mentioned before (see \autoref{implementation}).

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.8\textwidth]{img/algorithm_execution_times}
  \caption[Algorithm execution times]{Algorithm execution times (own image)}
\end{figure}

The results show that \propername{Flink} can handle the different implementations more or less equally good. The only two cases that stick out are \textit{dependent-pareto} and \textit{dependent-uniform}. The uniform distribution was chosen to have baseline and to test the effect of the corner case of \propername{TPUT} where a lot of elements have more or less the same values and a lot of elements have values that are larger than the threshold. The Zipf distribution is a common distribution in nature and also in technical contexts, e.g. websites and their visits are Zipf distributed. Basically very few items have very high values, a few items have moderate values and a lot of items have very small values.\\
The outlier in \textit{dependent-uniform} is due to execution inaccuracies -- there were rather large differences in execution time when executing the same implementation with the same data on the same cluster ranging from 500ms up to 3s.\\
Since the groups with the shortest and the longest execution times belong to the same algorithm the difference is most likely due to the data that they worked on -- respectively the distributions of the data. If executed with a uniformly distributed data set (\textit{dependent-uniform}) the phase two of the implementation returns a lot of elements -- which is the problematic corner case of \propername{TPUT} (see \autoref{sec:implementation:dependent}) -- and thus slowing the execution down as a whole. For Zipf distributed data the second phase returns few elements which makes this faster compared to uniformly distributed data and interestingly also every other execution of implementation and distribution combination. Even the execution times of the naive implementation are slower for all distributions than \textit{dependent-pareto} which is strange because the naive implementation is basically just the first step of three similar steps in the dependent implementation (compare \autoref{fig:implementation:naive} \& \autoref{fig:implementation:dependent}).\\
Measurements for uniformly distributed data for the iterative implementation are missing because the execution time would have been way off chart. $26^3$ elements means that the values of the $k$th highest element is most likely equal to the value of the first element. In a uniformly distributed data set elements with higher values than the threshold outnumber the ones with values below the threshold by the number of nodes to 1 -- in the test cases $9:1$. But in each iteration only $k$ elements are fetched from each partition so it takes a lot of iterations to reach the threshold ($26^3 \times\; ^8/_9\; / k$ iterations to be precise -- it has to see $^8/_9$ of the data set with a window size of $k$). The overhead to start one iteration is rather small but it increases over time because the ever growing aggregated list is distributed to the worker nodes at the beginning of each of their iterations.

\begin{table}[h]\footnotesize
  \centering
  \begin{tabular}{l | c}
  	Algorithm & Transferred elements\\
  	\hline
  	Naive implementation & $\sim 26^3 \times 10$ \\
  	Iterative implementation & $\sim 26^3 \times 18$ \\
    Dependent tasks & $\sim 26^3 \times 22$ \\
  \end{tabular}
  \caption{Transferred data as noted in the \propername{Flink} cluster web interface}
\end{table}

The amounts of transferred elements show that the approaches to build implementations that send less data than the naive baseline algorithm actually did not only not succeed but fail. This is due to the several problems that were mentioned for the different implementations before (see \autoref{implementation}). Remarkably the notable difference in transferred items is not reflected in differences in execution times -- \propername{Flink} seems to do a good job, but it would probably have an effect with even more data.

\section{Spark algorithm}

The \propername{Spark} algorithm could not be tested on a cluster so it would not be fair to compare the execution time to the execution times of the \propername{Flink} implementations. One thing is remarkable though: The warm-up phase of the RDDs is reflected in the execution times. The first query on the cached RDD takes $2s$, the second one $0.5s$ and the third $0.3s$ ($\sim\;^1/_7$). So the unique selling point of the framework works like a charm.\\

\begin{table}[h]\footnotesize
	\centering
	\begin{tabular}{l | c | c}
		$\alpha$ & $0.5$ & $1.0$ \\
		\hline
		Uniform & $\sim\;^{17}/_{18} \times 26^3 \times 9$ & $\sim\;^8/_9 \times 26^3 \times 9$ \\
		Mixed & $\sim 130 $ & $\sim 250$ \\
		Zipf & $\sim 270 $ & $\sim 750$ \\
	\end{tabular}
	\caption{Transferred data in relation to pruning parameter $\alpha$}
\end{table}

For uniformly distributed data the metrics read around the same $^8/_9 \times 26^3 \times 9$ of data so they again proof the problematic case of the algorithm. Setting the pruning parameter $\alpha$ to $0.5$ as recommended \cite[4,11]{Cao:2004:ETQ:1011767.1011798} increases the amount of sent data but this is due to the artificial testing data and the fact that every key exists in every partition. But for rather normally distributed data setting $\alpha=0.5$ seems to make sense.\\
Overall the \propername{Spark} implementation outperforms the \propername{Flink} implementations by far. The difference is very small for the uniformly distributed data but is significant for mixed and Zipf distributed data. No statement can be made in regard to execution times because the implementations ran on different setups.